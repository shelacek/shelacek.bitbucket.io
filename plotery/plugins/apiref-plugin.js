const selector = 'pre[data-lang="jsx"] .class-name';

export const apirefPlugin = (hook, vm) => hook.doneEach(async () => {
	const elements = document.querySelectorAll(selector);
	if (!elements.length || !vm.config.apiref) {
		return;
	}
	const apiMarkdown = await Docsify.get(
		vm.router.getFile(vm.config.apiref),
		false,
		vm.config.requestHeaders
	);
	const refs = Array.from(
		apiMarkdown.matchAll(/^#+ [^\n]+? :id=(apiref-[a-z0-9_-]+)$/mg),
		x => x[1]
	);
	for (const el of elements) {
		const ref = `apiref-${el.textContent.toLowerCase()}`;
		if (refs.indexOf(ref) !== -1) {
			const link = document.createElement('a');
			link.href = vm.router.toURL(vm.config.apiref, { id: ref });
			el.parentNode.insertBefore(link, el);
			link.appendChild(el);
		}
	}
});
