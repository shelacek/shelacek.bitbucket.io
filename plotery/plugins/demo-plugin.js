import htm from 'https://esm.sh/htm@3.1.1';
import { h, render, Component } from 'https://esm.sh/preact@10.22.0';
import { Chart, LinearAxis, CardinalLine } from 'https://esm.sh/@shelacek/plotery@1.1.1?deps=preact@10.22.0';

const html = htm.bind(h);

export const demoPlugin = hook => hook.doneEach(() => {
	const target = document.querySelector('.cover');

	if (!target.classList.contains('show')) {
		const el = target.querySelector('.plotery');
		el && render(null, target, el);
		return;
	}

	if (target.matches('.plotery')) {
		return;
	}

	class DemoChart extends Component {
		constructor() {
			super();
			const zeros = Array.from({ length: 10 }, (_, i) => [i, 0]);
			this.state = { data: { a: zeros, b: zeros } };
		}

		componentDidMount() {
			this.timer = setTimeout(() => this.update(), 100);
		}

		componentWillUnmount() {
			clearInterval(this.timer);
		}

		update() {
			const a = Array.from(
				{ length: 10 },
				(_, i) => [i, 0.7 + Math.random()
			]);
			this.setState(state => ({ data: { a, b: state.data.a } }));
			this.timer = setTimeout(() => this.update(), 2000);
		}

		render({}, { data }) {
			return html`
				<${Chart} data=${data}>
					<defs>
						<linearGradient id="demo-gradient" x1="0" x2="0" y1="0" y2="1">
							<stop offset="0" stop-color="var(--color-stop)" stop-opacity="1"></stop>
							<stop offset="0.7" stop-color="var(--color-stop)" stop-opacity="0"></stop>
						</linearGradient>
					</defs>
					<${LinearAxis} type="x" min=${0} max=${9} hide />
					<${LinearAxis} type="y" min=${0} max=${2} hide />
					<${CardinalLine} series="a" area />
					<${CardinalLine} series="b" area />
				<//>
			`;
		}
	}

	render(html`<${DemoChart} />`, target);
});
