import htm from 'https://esm.sh/htm@3.1.1';
import * as preact from 'https://esm.sh/preact@10.22.0';
import * as plotery from 'https://esm.sh/@shelacek/plotery@1.1.1?deps=preact@10.22.0';

const htmPreact = { ...preact, html: htm.bind(preact.h) };

const replaceMap = [
	[
		/import \{(.+?)\} from 'htm\/preact'/,
		'const {$1} = htmPreact'
	], [
		/import \{(.+?)\} from '@shelacek\/plotery'/,
		'const {$1} = plotery'
	], [
		/render\((.+?), document\.body\)/,
		'render($1, el)'
	]
];

const selector = 'pre[data-lang="preview"]';

export const previewPlugin = hook => hook.doneEach(() => {
	for (const target of document.querySelectorAll(selector)) {
		const content = replaceMap.reduce(
			(acc, x) => acc.replace(...x),
			target.querySelector('code').textContent
		);
		const el = document.createElement('div');
		el.classList.add('preview');
		target.parentNode.replaceChild(el, target);
		const func = new Function(
			'htmPreact',
			'plotery',
			'el',
			content
		);
		func(htmPreact, plotery, el);
	}
});
