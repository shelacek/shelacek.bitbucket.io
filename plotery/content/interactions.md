# Interactions

Interactions in Plotery are based on [&lt;Pointer /&gt;](api.md?id=apiref-pointer)
and [&lt;Wheel /&gt;](api.md?id=apiref-wheel) components that register event listeners
on the Plotery root SVG element. Panning and zooming are based on manipulation with axis limits.
Interacting with the data points, sections, and other elements on charts is done via the comparison
of pointer coordinates with input data. Below are examples with built-in components for most common
cases.

?> **Please note:** Plotery charts are nothing more than SVG images. For simple highlighting of data
series, you can use CSS with `:hover` pseudo-class. Turning series on/off can be easily done
by applying a class to [&lt;LinearLine /&gt;](api.md?id=apiref-linearline) (and friends) to hide
them or exclude series from the input data completely.


## Box zoom

Enables drawing zoom box with pointer device over chart canvas. Callback return array with dragged
rect in format `[x0, y0, x1, y1]`. Single click without dragging returns `null`.

```jsx
handleLimits = limits => this.setState({ limits });

// ...

<Chart data={data}>
	<LinearAxis type="x" min={limits ? limits[0] : 0} max={limits ? limits[2] : 5} major />
	<LinearAxis type="y" min={limits ? limits[1] : -1.5} max={limits ? limits[3] : 1.5} major />
	<LinearLine area />
	<BoxZoom onLimits={this.handleLimits} />
</Chart>
```

Try left-click and drag on the plot to zoom to selection and use single click to reset zoom.

```preview
import { html, render, Component } from 'htm/preact';
import { Chart, LinearAxis, LinearLine, BoxZoom } from '@shelacek/plotery';
let value = 0;
const data = Array.from({ length: 501 }, (_, i) => {
	const diff = Math.random() * 20 - 10;
	value += diff;
	value = (value < -100 || value > 100) ? value - 2 * diff : value;
	return [i, value];
});
class App extends Component {
	handleLimits = limits => this.setState({ limits });
	render = ({}, { limits }) => html`
		<${Chart} data=${data}>
			<${LinearAxis} type="x" min=${limits ? limits[0] : 0} max=${limits ? limits[2] : 500} major />
			<${LinearAxis} type="y" min=${limits ? limits[1] : -100} max=${limits ? limits[3] : 100} major />
			<${LinearLine} area />
			<${BoxZoom} onLimits=${this.handleLimits} />
		<//>
	`;
}
render(html`<${App} />`, document.body);
```

[&lt;BoxZoom /&gt;](api.md?id=apiref-boxzoom) also support one-dimensional selection when selection
is possible only along one axis. Use `restrict="x"` to restrict selection on `x` axis and
`restrict="y"` to restrict on `y` axis.

Panning and zooming usually need some boundaries. If this is the case, you can easily add some logic
to `onLimits` handler.


## Wheel zoom

Enables zooming with mouse wheel or touchpad.

Usage is same as [&lt;Pan /&gt;](api.md?id=apiref-pan) component with which it is often used
together. Please see example in [Panning](interactions.md?id=panning).

!> [&lt;WheelZoom /&gt;](api.md?id=apiref-wheelzoom) and [&lt;Pan /&gt;](api.md?id=apiref-pan)
doesn't have any build-in mechanism for reseting axis. If you need one, you can, for example,
add a button to set limits to null in its onclick handler.


## Panning

Enables moving with chart axis.

Next example show one-dimensional combined pan & zoom where zooming and padding are possible only
on x axis.

```jsx
handleLimits = limits => this.setState({ limits });

// ...

<Chart data={data}>
	<LinearAxis type="x" min={limits ? limits[0] : 0} max={limits ? limits[2] : 5} major />
	<LinearAxis type="y" min={-1.5} max={1.5} major />
	<LinearLine area />
	<Pan onLimits={this.handleLimits} />
	<WheelZoom modifiers="ctrl" onLimits={this.handleLimits} />
</Chart>
```

Use ctrl + mouse wheel to zoom around the pointer and drag with left-click to pan chart by x axis.

```preview
import { html, render, Component } from 'htm/preact';
import { Chart, LinearAxis, LinearLine, Pan, WheelZoom } from '@shelacek/plotery';
let value = 0;
const data = Array.from({ length: 2001 }, (_, i) => {
	const diff = Math.random() * 20 - 10;
	value += diff;
	value = (value < -100 || value > 100) ? value - 2 * diff : value;
	return [i - 850, value];
});
class App extends Component {
	state = { limits: null };
	handleLimits = limits => this.setState({ limits });
	render = ({}, { limits }) => html`
		<${Chart} data=${data}>
			<${LinearAxis} type="x" min=${limits ? limits[0] : 0} max=${limits ? limits[2] : 300} major />
			<${LinearAxis} type="y" min=${-100} max=${100} major />
			<${LinearLine} area />
			<${Pan} onLimits=${this.handleLimits} />
			<${WheelZoom} modifiers="ctrl" onLimits=${this.handleLimits} />
		<//>
	`;
}
render(html`<${App} />`, document.body);
```


## Tooltips

[&lt;Tooltip /&gt;](api.md?id=apiref-tooltip) is helper component for displaying tooltips.
It accepts single children that take cursor `position` (`[x, y]`) and does actual drawing of
tooltip. Library doesn't contains any ready-to-use tooltip display, because the requirements for
tooltips vary wildly depending on the chart type, input data and preferences on the displaying
itself. But it's not that hard to create your own tooltip:

```jsx
const CartesianPointTooltip = ({ rect, axes, data, position }) => {
	if (!data || !data.length || !axes.x || !axes.y) {
		return null;
	}
	const pos = axes.x.scale(position[0], true);
	const diffs = points.map(x => Math.abs(pos - x[0]));
	const point = points[diffs.findIndex(x => x === Math.min(...diffs))];
	return (
		<g>
			<path d={`M${position[0]},0V${rect.height}`} stroke-width="1px" stroke="gray" stroke-dasharray="4" />
			<circle cx={axes.x.scale(point[0])} cy={axes.y.scale(point[1])} r="3" stroke-width="2px" stroke="black" fill="white" />
			<text x={axes.x.scale(point[0])} y="-7" text-anchor="middle" font-weight="bold">{point[1].toFixed(2)}</text>
		</g>
	);
};

// ...

<Chart data={sin}>
	<LogAxis type="x" min={1} max={100} major minor />
	<LinearAxis type="y" min={-1.5} max={1.5} major />
	<LinearLine area />
	<Tooltip><CartesianPointTooltip /></Tooltip>
</Chart>
```

The example `<CartesianPointTooltip />` isn't very performant or flexible, but presents simple
tooltip, that shows vertical line on cursor position, nearest data-point and its `y` value:

```preview
import { html, render, Component } from 'htm/preact';
import { Chart, LinearAxis, LogAxis, LinearLine, Tooltip } from '@shelacek/plotery';
const sin = Array.from({ length: 100 }, (_, i) => [1 + i, Math.sin(i / 10)]);
const CartesianPointTooltip = ({ rect, axes, data, position }) => {
	if (!data || !data.length || !axes.x || !axes.y) {
		return null;
	}
	const pos = axes.x.scale(position[0], true);
	const diffs = data.map(x => Math.abs(pos - x[0]));
	const point = data[diffs.findIndex(x => x === Math.min(...diffs))];
	return html`
		<g>
			<path d="M${position[0]},0V${rect.height}" stroke-width="1px" stroke="gray" stroke-dasharray="4" />
			<circle cx=${axes.x.scale(point[0])} cy=${axes.y.scale(point[1])} r="3" stroke-width="2px" stroke="black" fill="white" />
			<text x=${axes.x.scale(point[0])} y="-7" text-anchor="middle" font-weight="bold">${point[1].toFixed(2)}</text>
		</g>
	`;
};
const App = () => html`
	<${Chart} data=${sin}>
		<${LogAxis} type="x" min=${1} max=${100} major minor />
		<${LinearAxis} type="y" min=${-1.5} max=${1.5} major />
		<${LinearLine} area />
		<${Tooltip}><${CartesianPointTooltip} /><//>
	<//>
`;
render(html`<${App} />`, document.body);
```
