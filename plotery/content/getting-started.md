# Getting started

Plotery is Preact library that can be run in a browser and supports SSR.


## Instalation

Plotery can be installed with NPM or YARN:

```bash
npm i @shelacek/plotery
# or
yarn add @shelacek/plotery
```

You can also obtain the library from CDN like [jsDelivr](https://www.jsdelivr.com/package/npm/@shelacek/plotery)
or [unpkg](https://unpkg.com/@shelacek/plotery/).


## Integration

Plotery can be used with bundlers like Webpack. This is usually the case because JSX needs to be
transpiled to javascript before it can be run in the browser. Plotery export classes and functions
as ECMAScript modules, that can be imported via named imports:

```js
import { Chart, LinearAxis, LinearLine /* ... */ } from '@shelacek/plotery';
```


### Styles

Plotery uses CSS to style its charts, so you must include it to your bundle or completly roll up
your own stylesheet. You can use SCSS from `dist/scss/plotery.scss` (prefered) or include CSS
to page:

```html
<link rel="stylesheet" href="path-to-plotery/dist/css/plotery.css">
```


### Use without bundlers

Plotery does not need to be transpiled, so you can alternatively load it from CDN and
execute directly in the browser without any build step.

?> You can check [HTM](https://github.com/developit/htm). HTM is useful if you don't use JSX and
still prefer JSX-like syntax instead of plain javascript. This is the route, that this site use.

The single 30 lines HTML example that uses Plotery from CDN, utilize HTM instead of JSX, and run in
the browser is on [CodeSandbox](https://codesandbox.io/s/simple-chart-use-without-builders-h3r3h1).


## Data format

Data must be provided in the following format:

```ts
type ChartData = ChartSeriesData | { [series: string]: ChartSeriesData };
```

If `PloteryData` is an object, then chart primitives, like `LinearLine` must be provided with
`series` props that select single `ChartSeriesData`.

The `ChartSeriesData` type depends on the chart primitive where it is used, but for all build-in
primitives are an array of `number` pairs:

- `[x, y]` for cartesian lines
- `[radial, theta]` for polar lines
- `[theta1, theta2]` for polar sectors


## First chart

This code create chart from the [title page](/?id=talk-is-cheap-show-me-the-code):

```jsx
// import used components/functions
import { h, render } from 'preact';
import { Chart, LinearAxis, LinearLine } from '@shelacek/plotery';

// generate data series for chart: y = cos(2^x)
const cos2powx = Array.from({ length: 501 }, (_, i) => [i / 100, Math.cos(Math.pow(2, i / 100))]);

// define chart component
const App = () => (
	<Chart data={cos2powx}>
		<LinearAxis type="x" min={0} max={5} major />
		<LinearAxis type="y" min={-1.5} max={1.5} major />
		<LinearLine />
	</Chart>
);

// render component into document
render(<App />, document.body);
```

Try it on [CodeSandbox](https://codesandbox.io/s/simple-chart-kg7297).
