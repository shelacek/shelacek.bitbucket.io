# Plotery <!-- {docsify-ignore-all} -->

> 5kB declarative SVG-based charting library for Preact

There are many plotting libraries in the wild. Many of them are enormous, all capable or slow.
If there is some lightweight library, then it is usually poorly extensible. *Plotery* try to be
different - small, contains the bare essentials and it is fast. I simply need a library that is
capable draw thousands of points in real-time and which would fit into a small embedded device with
a few spare kilobytes.

!> If you need a charting library, that magically render any arbitrary data that you put in,
*Plotery* is probably not for you.


## Features

- Plotery generate SVG markup and let the browser do the rest
- Declarative configuration: It's Preact library, so why not use JSX?
- [Small footprint](https://bundlephobia.com/package/@shelacek/plotery): don't burn bits for
  sporadically used features
- Dependency-free, but easily extensible
- Styled with CSS


## Talk is cheap. Show me the code.

All right, here it is:

```jsx
<Chart data={cos2powx}>
	<LinearAxis type="x" min={0} max={5} major />
	<LinearAxis type="y" min={-1.5} max={1.5} major />
	<LinearLine />
</Chart>
```

```preview
import { html, render } from 'htm/preact';
import { Chart, LinearAxis, LinearLine } from '@shelacek/plotery';
const cos2powx = Array.from({ length: 501 }, (_, i) => [i / 100, Math.cos(Math.pow(2, i / 100))]);
const App = () => html`
	<${Chart} data=${cos2powx}>
		<${LinearAxis} type="x" min=${0} max=${5} major />
		<${LinearAxis} type="y" min=${-1.5} max=${1.5} major />
		<${LinearLine} />
	<//>
`;
render(html`<${App} />`, document.body);
```
