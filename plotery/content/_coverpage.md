![logo](../logomark.svg)

# Plotery <small>1.1.0</small>

> 5kB declarative SVG-based charting library for Preact

<a href="javascript:window.scrollTo({ top: window.innerHeight, behavior: 'smooth' });">Documentation</a>
[Bitbucket](https://bitbucket.org/shelacek/plotery)
