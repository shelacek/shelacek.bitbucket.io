# Chart types

This page contains examples of various chart types. Take the look to [API](api.md) for more details and
options.

!> This page is a stub. Feel free to
[improve it](https://bitbucket.org/shelacek/shelacek.bitbucket.io/src/master/plotery).


## Cartesian charts

Cartesian charts are charts that use a cartesian coordinate system.


### Line chart

[Ballmer Peak](https://xkcd.com/323):

```jsx
const data = [[0, 1], [0.03, 0.85], [0.121, 0.2], [0.1337, 2], [0.154, 0.25], [0.3, 0.02]];
const labels = ['WinME coders', 'Trained monkey', 'Normal', 'Citizens of Thuban 9', 'Superhuman'];

// ...

<Chart data={data}>
	<LinearAxis type="x" min={0} max={0.3} labels={x => `${x}%`} major minor />
	<LinearAxis type="y" min={0} max={2} step={0.5} labels={labels} major />
	<CardinalLine area tension={1} />
<Chart>
```

```preview
import { html, render } from 'htm/preact';
import { Chart, LinearAxis, CardinalLine } from '@shelacek/plotery';
const data = [[0, 1], [0.03, 0.85], [0.121, 0.2], [0.1337, 2], [0.154, 0.25], [0.3, 0.02]];
const labels = ['WinME coders', 'Trained monkey', 'Normal', 'Citizens of Thuban 9', 'Superhuman'];
const App = () => html`
	<${Chart} data=${data}>
		<${LinearAxis} type="x" min=${0} max=${0.3} labels=${x => `${x}%`} major minor />
		<${LinearAxis} type="y" min=${0} max=${2} step=${0.5} labels=${labels} major />
		<${CardinalLine} area tension=${1} />
	<//>
`;
render(html`<${App} />`, document.body);
```

### Log scale line chart

Gain to the normalized frequency response of 1st, 2nd, and 3th order type I Chebyshev filter with
ripple factor 1:

```jsx
const data = { n1: chebyshev(1, 1), n2: chebyshev(2, 1), n3: chebyshev(3, 1) };

// ...

<Chart data={data}>
	<LogAxis type="x" min={0.1} max={100} major minor />
	<LinearAxis type="y" min={0} max={1} step={0.2} major />
	<CardinalLine series="n1" />
	<CardinalLine series="n2" />
	<CardinalLine series="n3" />
</Chart>
```

```preview
import { html, render } from 'htm/preact';
import { Chart, LogAxis, LinearAxis, CardinalLine } from '@shelacek/plotery';
const chebyshev = (n, epsilon) => Array.from({ length: 999 }, (_, i) => {
	const omega = 0.1 + i / 10;
	const t = Array.from({ length: n })
		.reduce(acc => [acc[1], 2 * omega * acc[1] - acc[0]], [1, omega])[0];
	return [omega, 1 / Math.sqrt(1 + epsilon * epsilon * t * t)];
});
const data = { n1: chebyshev(1, 1), n2: chebyshev(2, 1), n3: chebyshev(3, 1) };
const App = () => html`
	<${Chart} data=${data}>
		<${LogAxis} type="x" min=${0.1} max=${100} major minor />
		<${LinearAxis} type="y" min=${0} max=${1} step=${0.2} major />
		<${CardinalLine} series="n1" />
		<${CardinalLine} series="n2" />
		<${CardinalLine} series="n3" />
	<//>
`;
render(html`<${App} />`, document.body);
```

### Bar chart

Poisson distribution with lambda 3, 9 and 27:

```jsx
const pois = { p3: [/*...*/], p9: [/*...*/], p27: [/*...*/] };

// ...

<Chart data={pois}>
	<LinearAxis type="x" min={-0.5} max={39.5} major minor />
	<LinearAxis type="y" min={0} max={0.25} major />
	<BarLine series="p3" />
	<BarLine series="p9" />
	<BarLine series="p27" />
</Chart>
```

```preview
import { html, render } from 'htm/preact';
import { Chart, LinearAxis, BarLine } from '@shelacek/plotery';
const pois = { p3: [], p9: [], p27: [] };
let fact = 0;
for (let k = 0; k < 40; k++) {
	fact = k * fact || 1;
	for (let [series, lambda] of Object.entries({ p3: 3, p9: 9, p27: 27 })) {
		pois[series].push([k, Math.pow(lambda, k) * Math.pow(Math.E, -lambda) / fact]);
	}
}
const App = () => html`
	<${Chart} data=${pois}>
		<${LinearAxis} type="x" min=${-0.5} max=${39.5} major minor />
		<${LinearAxis} type="y" min=${0} max=${0.25} major />
		<${BarLine} series="p3" />
		<${BarLine} series="p9" />
		<${BarLine} series="p27" />
	<//>
`;
render(html`<${App} />`, document.body);
```

### Mixed chart

Rise and fall of Firefox. **STOP CHROMIUM MONOPOLY!**

```jsx
const data = {
	share: [[2007 + 4 / 12, 25], [2007 + 5 / 12, 25.4], /*...*/, [2022 + 7 / 12, 3.5]],
	diff: [[2008, 4.2], [2009, 2], /*...*/, [2022, -0.5]]
};

// ...

<Chart data={data}>
	<LinearAxis type="x" min={2007 + 4 / 12} max={2022 + 4 / 12} step={3} divisor={3} major minor />
	<LinearAxis type="y" min={-6} max={35} labels={x => `${x}%`} major />
	<LinearLine series="share" area />
	<BarLine series="diff" style="stroke-width: 5%" />
</Chart>
```

```preview
import { html, render } from 'htm/preact';
import { Chart, LinearAxis, LinearLine, BarLine } from '@shelacek/plotery';
const data = {
	share: [
		[2007 + 4 / 12, 25], [2007 + 5 / 12, 25.4], [2007 + 6 / 12, 25.4], [2007 + 7 / 12, 25.8],
		[2007 + 8 / 12, 25.9], [2007 + 9 / 12, 26.6], [2007 + 10 / 12, 28.3], [2007 + 11 / 12, 28.5],
		[2008 + 0 / 12, 29.1], [2008 + 1 / 12, 29.2], [2008 + 2 / 12, 29.3], [2008 + 3 / 12, 29.3],
		[2008 + 4 / 12, 29.4], [2008 + 5 / 12, 29.6], [2008 + 6 / 12, 30.4], [2008 + 7 / 12, 32.4],
		[2008 + 8 / 12, 33.4], [2008 + 9 / 12, 31.2], [2008 + 10 / 12, 31.6], [2008 + 11 / 12, 32],
		[2009 + 0 / 12, 32.1], [2009 + 1 / 12, 32.3], [2009 + 2 / 12, 32.5], [2009 + 3 / 12, 32.5],
		[2009 + 4 / 12, 32.2], [2009 + 5 / 12, 32.4], [2009 + 6 / 12, 32.4], [2009 + 7 / 12, 33],
		[2009 + 8 / 12, 32.9], [2009 + 9 / 12, 33.1], [2009 + 10 / 12, 33], [2009 + 11 / 12, 33],
		[2010 + 0 / 12, 32.9], [2010 + 1 / 12, 32.8], [2010 + 2 / 12, 33.1], [2010 + 3 / 12, 32.7],
		[2010 + 4 / 12, 33.5], [2010 + 5 / 12, 33.6], [2010 + 6 / 12, 34.1], [2010 + 7 / 12, 33.4],
		[2010 + 8 / 12, 31.9], [2010 + 9 / 12, 31.7], [2010 + 10 / 12, 32.2], [2010 + 11 / 12, 31.2],
		[2011 + 0 / 12, 31.9], [2011 + 1 / 12, 31], [2011 + 2 / 12, 30.9], [2011 + 3 / 12, 30.7],
		[2011 + 4 / 12, 29.4], [2011 + 5 / 12, 29], [2011 + 6 / 12, 28.5], [2011 + 7 / 12, 27.6],
		[2011 + 8 / 12, 27.1], [2011 + 9 / 12, 26.9], [2011 + 10 / 12, 26.4], [2011 + 11 / 12, 26.3],
		[2012 + 0 / 12, 25.5], [2012 + 1 / 12, 25.2], [2012 + 2 / 12, 25.2], [2012 + 3 / 12, 25],
		[2012 + 4 / 12, 24.3], [2012 + 5 / 12, 23.2], [2012 + 6 / 12, 23.2], [2012 + 7 / 12, 23],
		[2012 + 8 / 12, 23], [2012 + 9 / 12, 22.2], [2012 + 10 / 12, 21.4], [2012 + 11 / 12, 20.2],
		[2013 + 0 / 12, 20.1], [2013 + 1 / 12, 19.8], [2013 + 2 / 12, 19.6], [2013 + 3 / 12, 20.9],
		[2013 + 4 / 12, 19.8], [2013 + 5 / 12, 19.8], [2013 + 6 / 12, 19.1], [2013 + 7 / 12, 18.6],
		[2013 + 8 / 12, 17.6], [2013 + 9 / 12, 16.9], [2013 + 10 / 12, 17.2], [2013 + 11 / 12, 18.1],
		[2014 + 0 / 12, 18.4], [2014 + 1 / 12, 18.3], [2014 + 2 / 12, 18.2], [2014 + 3 / 12, 18.6],
		[2014 + 4 / 12, 17.7], [2014 + 5 / 12, 16.9], [2014 + 6 / 12, 15.3], [2014 + 7 / 12, 15.5],
		[2014 + 8 / 12, 16.4], [2014 + 9 / 12, 16.1], [2014 + 10 / 12, 16.5], [2014 + 11 / 12, 15.7],
		[2015 + 0 / 12, 15.2], [2015 + 1 / 12, 14.9], [2015 + 2 / 12, 14.7], [2015 + 3 / 12, 14.5],
		[2015 + 4 / 12, 14.3], [2015 + 5 / 12, 14.4], [2015 + 6 / 12, 13.3], [2015 + 7 / 12, 13.4],
		[2015 + 8 / 12, 12.5], [2015 + 9 / 12, 11.8], [2015 + 10 / 12, 11.6], [2015 + 11 / 12, 11.4],
		[2016 + 0 / 12, 11.4], [2016 + 1 / 12, 12], [2016 + 2 / 12, 10], [2016 + 3 / 12, 9.7],
		[2016 + 4 / 12, 10], [2016 + 5 / 12, 10.9], [2016 + 6 / 12, 10.1], [2016 + 7 / 12, 12.4],
		[2016 + 8 / 12, 13.1], [2016 + 9 / 12, 10.4], [2016 + 10 / 12, 11.1], [2016 + 11 / 12, 10.4],
		[2017 + 0 / 12, 9.5], [2017 + 1 / 12, 9], [2017 + 2 / 12, 7.2], [2017 + 3 / 12, 6.3],
		[2017 + 4 / 12, 9], [2017 + 5 / 12, 7.8], [2017 + 6 / 12, 6.9], [2017 + 7 / 12, 8.1],
		[2017 + 8 / 12, 9.1], [2017 + 9 / 12, 9.1], [2017 + 10 / 12, 9.3], [2017 + 11 / 12, 9.4],
		[2018 + 0 / 12, 9.2], [2018 + 1 / 12, 8.6], [2018 + 2 / 12, 7.2], [2018 + 3 / 12, 6.5],
		[2018 + 4 / 12, 6.6], [2018 + 5 / 12, 5.5], [2018 + 6 / 12, 6.1], [2018 + 7 / 12, 7.4],
		[2018 + 8 / 12, 7.2], [2018 + 9 / 12, 6.6], [2018 + 10 / 12, 6.7], [2018 + 11 / 12, 6.3],
		[2019 + 0 / 12, 6.3], [2019 + 1 / 12, 6.5], [2019 + 2 / 12, 6.3], [2019 + 3 / 12, 6.1],
		[2019 + 4 / 12, 6.8], [2019 + 5 / 12, 6.8], [2019 + 6 / 12, 6.5], [2019 + 7 / 12, 5.3],
		[2019 + 8 / 12, 6.3], [2019 + 9 / 12, 6.9], [2019 + 10 / 12, 6.1], [2019 + 11 / 12, 5.5],
		[2020 + 0 / 12, 5.5], [2020 + 1 / 12, 5.4], [2020 + 2 / 12, 4.5], [2020 + 3 / 12, 4.6],
		[2020 + 4 / 12, 4.5], [2020 + 5 / 12, 5.1], [2020 + 6 / 12, 5], [2020 + 7 / 12, 4.8],
		[2020 + 8 / 12, 4.8], [2020 + 9 / 12, 4.6], [2020 + 10 / 12, 4.5], [2020 + 11 / 12, 4.4],
		[2021 + 0 / 12, 4.1], [2021 + 1 / 12, 4.3], [2021 + 2 / 12, 4.4], [2021 + 3 / 12, 4.1],
		[2021 + 4 / 12, 3.2], [2021 + 5 / 12, 3.3], [2021 + 6 / 12, 2.9], [2021 + 7 / 12, 4.8],
		[2021 + 8 / 12, 5.8], [2021 + 9 / 12, 3.6], [2021 + 10 / 12, 3.2], [2021 + 11 / 12, 3.2],
		[2022 + 0 / 12, 3.9], [2022 + 1 / 12, 3.2], [2022 + 2 / 12, 4.1], [2022 + 3 / 12, 2.8],
		[2022 + 4 / 12, 2.6], [2022 + 5 / 12, 3.8], [2022 + 6 / 12, 3.6], [2022 + 7 / 12, 3.5]
	],
	diff: [
		[2008, 4.2], [2009, 2], [2010, 0.2], [2011, -4], [2012, -5.3],
		[2013, -4.5], [2014, -2], [2015, -3.5], [2016, -2.5], [2017, -2.6],
		[2018, -1.4], [2019, -0.7], [2020, -1.5], [2021, -0.9], [2022, -0.5]
	]
};
const App = () => html`
	<${Chart} data=${data}>
		<${LinearAxis} type="x" min=${2007 + 4 / 12} max=${2022 + 4 / 12} step=${3} divisor=${3} major minor />
		<${LinearAxis} type="y" min=${-6} max=${35} labels=${x => `${x}%`} major />
		<${LinearLine} series="share" area />
		<${BarLine} series="diff" style="stroke-width: 5%" />
	<//>
`;
render(html`<${App} />`, document.body);
```

### Multiple axis chart

Amplitude and phase spectrum of delayed unit pulse:

```jsx
const ft = { amp: [/*...*/], pha: [/*...*/] };
const angles = ['π', 'π/2', '0', '-π/2', '-π'];

// ...

<Chart data={ft}>
	<LinearAxis type="x" min={-3} max={3} major />
	<Surface>
		<LinearAxis type="y" min={0} max={1} step={0.25} major minor />
		<LinearLine series="amp" />
	</Surface>
	<Surface>
		<LinearAxis type="y" min={-1} max={1} step={0.5} labels={angles} position="end" />
		<LinearLine series="pha" />
	</Surface>
</Chart>
```

```preview
import { html, render } from 'htm/preact';
import { Chart, Surface, LinearAxis, LinearLine } from '@shelacek/plotery';
const ft = { amp: [], pha: [] };
for (let x = -3; x <= 3; x += 0.03) {
	const re = Math.sin(2 * Math.PI * x) / (x * 2 * Math.PI);
	const im = (Math.cos(2 * Math.PI * x) - 1) / (x * 2 * Math.PI);
	ft.amp.push([x, Math.sqrt(re * re + im * im)]);
	ft.pha.push([x, Math.atan2(im, re) / Math.PI]);
}
const angles = ['-π', '-π/2', '0', 'π/2', 'π'];
const App = () => html`
	<${Chart} data=${ft}>
		<${LinearAxis} type="x" min=${-3} max=${3} major />
		<${Surface}>
			<${LinearAxis} type="y" min=${0} max=${1} step=${0.25} major minor />
			<${LinearLine} series="amp" />
		<//>
		<${Surface}>
			<${LinearAxis} type="y" min=${-1} max=${1} step=${0.5} labels=${angles} position="end" />
			<${LinearLine} series="pha" />
		<//>
	<//>
`;
render(html`<${App} />`, document.body);
```

## Polar charts

Polar charts are charts that use a polar coordinate system.


### Doughnut and pie chart

```jsx
const data = {
	y2020: [[0, 180], [180, 280], [280, 330], [330, 350], [350, 360]],
	y2021: [[0, 160], [160, 270], [270, 320], [320, 345], [345, 360]],
	y2022: [[0, 150], [150, 245], [245, 305], [305, 335], [335, 360]],
};

// ...

<Chart data={data} stroke-width="3" stroke="var(--base-background-color)">
	<RadialAxis min={0} max={100} hide />
	<AngularAxis min={0} max={360} hide />
	<PolarSector series="y2020" inner={25} outer={50} />
	<PolarSector series="y2021" inner={50} outer={75} />
	<PolarSector series="y2022" inner={75} outer={100} />
</Chart>
```

```preview
import { html, render } from 'htm/preact';
import { Chart, RadialAxis, AngularAxis, PolarSector } from '@shelacek/plotery';
const data = {
	y2020: [[0, 180], [180, 280], [280, 330], [330, 350], [350, 360]],
	y2021: [[0, 160], [160, 270], [270, 320], [320, 345], [345, 360]],
	y2022: [[0, 150], [150, 245], [245, 305], [305, 335], [335, 360]],
};
const App = () => html`
	<${Chart} data=${data} stroke-width="3" stroke="var(--base-background-color)">
		<${RadialAxis} min=${0} max=${100} hide />
		<${AngularAxis} min=${0} max=${360} hide />
		<${PolarSector} series="y2020" inner=${25} outer=${50} />
		<${PolarSector} series="y2021" inner=${50} outer=${75} />
		<${PolarSector} series="y2022" inner=${75} outer=${100} />
	<//>
`;
render(html`<${App} />`, document.body);
```

!> Legends are usually necessary for doughnut/pie charts. But legends are also subject of various
design requirements and Plotery does not contain any buildin legends. The good ol' HTML is your
friend.


### Polar Line chart

[Butterfly curve](https://en.wikipedia.org/wiki/Butterfly_curve_(transcendental)):

```jsx
const data = Array.from({ length: 1000 }, (_, i) => {
	// ...
	return [exp(sin(t)) - 2 * cos(4 * t) + sin5((2 * t - Math.PI) / 24), 180 * t / Math.PI - 90];
});

// ...

<Chart data={data}>
	<RadialAxis min={0} max={5} step={1} major />
	<AngularAxis min={0} max={360} major minor />
	<PolarLine />
</Chart>
```

```preview
import { html, render } from 'htm/preact';
import { Chart, RadialAxis, AngularAxis, PolarLine } from '@shelacek/plotery';
const data = Array.from({ length: 1000 }, (_, i) => {
	const exp = Math.exp;
	const sin = Math.sin;
	const cos = Math.cos;
	const sin5 = x => (10 * sin(x) - 5 * sin(3 * x) + sin(5 * x)) / 16;
	const t = 8 * Math.PI * (i - 500) / 500;
	return [
		exp(sin(t)) - 2 * cos(4 * t) + sin5((2 * t - Math.PI) / 24),
		180 * t / Math.PI - 90
	];
});
const App = () => html`
	<${Chart} data=${data}>
		<${RadialAxis} min=${0} max=${5} step=${1} major />
		<${AngularAxis} min=${0} max=${360} major minor />
		<${PolarLine} />
	<//>
`;
render(html`<${App} />`, document.body);
```
