# API

For full API of exported symbols in dense format, please see typings at
https://bitbucket.org/shelacek/plotery/src/master/src/types.d.ts.

Global components:

- [Chart component](#apiref-chart)
- [Surface component](#apiref-surface)

Cartesian chart components:

- [LinearAxis component](#apiref-linearaxis)
- [LogAxis component](#apiref-logaxis)
- [LinearLine component](#apiref-linearline)
- [CardinalLine component](#apiref-cardinalline)
- [BarLine component](#apiref-barline)

Polar chart components:

- [RadialAxis component](#apiref-radialaxis)
- [AngularAxis component](#apiref-angularaxis)
- [PolarLine component](#apiref-polarline)
- [PolarSector component](#apiref-polarsector)

Interaction components:

- [BoxZoom component](#apiref-boxzoom)
- [WheelZoom component](#apiref-wheelzoom)
- [Pan component](#apiref-pan)
- [Tooltip component](#apiref-tooltip)

Low-level components:

- [CartesianAxis component](#apiref-cartesianaxis)
- [CartesianLine component](#apiref-cartesianline)
- [PolarAxis component](#apiref-polaraxis)
- [Pointer component](#apiref-pointer)
- [Wheel component](#apiref-wheel)

Low-level functions:

- [Interpolation functions](#interpolation-functions)
- [Scaler functions](#scaler-functions)
- [Grid step estimation functions](#grid-step-estimation-functions)
- [Functions for generate grid ticks](#functions-for-generate-grid-ticks)


## Global components


### Chart component :id=apiref-chart

Root component for all **Plotery** charts.

#### Properties

| Prop     | Type                   | Default | Description                                                       |
| -------- | ---------------------- | ------- | ----------------------------------------------------------------- |
| `class`¹ | `string`               | none    | Space-separated list of the classes applied to plotery container. |
| `data`   | `ChartData` (required) |         | Charts series.                                                    |

*¹`class` and `className` are equivalent.*

Any other passed property will be added to SVG container.


#### Props passed to children

| Prop         | Type                       | Description                                        |
| ------------ | -------------------------- | -------------------------------------------------- |
| `data`       | `ChartData`                | Passed charts series.                              |
| `rect`       | `Rect`                     | Size and absolute position of chart root SVG.      |
| `host`       | `SVGElement`               | Chart root SVG element.                            |
| `updateAxis` | `(axis: Axis): void`       | See [Surface](api.md?id=apiref-surface) component. |
| `axes`       | `{ [type: string]: Axis }` | See [Surface](api.md?id=apiref-surface) component. |

Props `updateAxis` and `axes` are provided by `Chart`'s implicit [Surface](api.md?id=apiref-surface)
component.

#### DOM structure

```
div.plotery > svg[overflow="visible"]
```


### Surface component :id=apiref-surface

Encapsulates the coordinate system. Allows defining an axis for `Surface` scope, so series defined
in that scope use that axis. `Chart` introduce `Surface` implicitly.

#### Properties

*No public properties*

#### Props passed to children

| Prop         | Type                            | Description                                   |
| ------------ | ------------------------------- | --------------------------------------------- |
| `updateAxis` | `(axis: Axis): void` (internal) | Register or update axis for current surface.  |
| `axes`       | `{ [type: string]: Axis }`      | Object with scoped surface axes.              |

Function `updateAxis` should be called only from axis to register or update itself. 


## Cartesian chart components

### LinearAxis component :id=apiref-linearaxis

Define linear cartesian axis. Use [&lt;CartesianAxis /&gt;](api.md?id=apiref-cartesianaxis) under
the hood.

#### Properties

| Prop        | Type                                    | Default                                | Description                                                                   |
| ----------- | --------------------------------------- | -------------------------------------- | ----------------------------------------------------------------------------- |
| `class`¹    | `string`                                | none                                   | Space-separated list of the classes applied to axis container.                |
| `type`      | `'x' ⎮ 'y'` (required)                  |                                        | Type of axis.                                                                 |
| `min`       | `number` (required)                     |                                        | Low axis limit.                                                               |
| `max`       | `number` (required)                     |                                        | High axis limit.                                                              |
| `position`  | `'start' ⎮ 'end'`                       | `'start'` for `'x'`, `'end'` for `'y'` | Position of axis labels.                                                      |
| `reference` | `number`                                | `0`                                    | Reference value (origin).                                                     |
| `step`      | `number`                                | *calculated from `min` and `max`*      | Step between consequence major ticks.                                         |
| `divisor`   | `number`                                | `5`                                    | How many minor ticks are in one major tick.                                   |
| `major`     | `boolean`                               | `false`                                | Show major grid.                                                              |
| `minor`     | `boolean`                               | `false`                                | Show minor grid.                                                              |
| `labels`    | `string[] ⎮ { (tick: number): string }` | `x => ‘${x}‘`                          | Labels for axis. Array of label strings must have same length as major ticks. |
| `hide`      | `boolean`                               | `false`                                | Don't display axis (it will be used only for coordinate system).              |

*¹`class` and `className` are equivalent.*

#### Methods

- `get type(): string`  
  Return passed `type` prop; 'x' or 'y'.
- `get reference(): number`  
  Return passed `reference` prop.
- `scale(value: number, reverse: boolean = false): number`  
  Convert number from axis coordinate to absolute viewport coordinate. If `reverse` is true,
  function convert absolute to axis coordinates.

#### DOM structure

```
g.axis.cartesian > (path.grid.major + path.grid.minor + path.reference + g.labels > text*)
```


### LogAxis component :id=apiref-logaxis

Define logarithmic cartesian axis. Use [&lt;CartesianAxis /&gt;](api.md?id=apiref-cartesianaxis)
under the hood.

#### Properties

| Prop        | Type                                    | Default                                   | Description                                                                   |
| ----------- | --------------------------------------- | ----------------------------------------- | ----------------------------------------------------------------------------- |
| `class`¹    | `string`                                | none                                      | Space-separated list of the classes applied to axis container.                |
| `type`      | `'x' ⎮ 'y'` (required)                  |                                           | Type of axis.                                                                 |
| `min`       | `number` (required)                     |                                           | Low axis limit.                                                               |
| `max`       | `number` (required)                     |                                           | High axis limit.                                                              |
| `position`  | `'start' ⎮ 'end'`                       | `'start'` for `'x'`, `'end'` for `'y'`    | Position of axis labels.                                                      |
| `reference` | `number`                                | `1`                                       | Reference value (origin).                                                     |
| `base`      | `number`                                | `10`                                      | Logaritmic base for scaling.                                                  |
| `step`      | `number`                                | *calculated from `min`, `max` and `base`* | Step between consequence major ticks.                                         |
| `divisor`   | `number`                                | `9`                                       | How many minor ticks are in one major tick.                                   |
| `major`     | `boolean`                               | `false`                                   | Show major grid.                                                              |
| `minor`     | `boolean`                               | `false`                                   | Show minor grid.                                                              |
| `labels`    | `string[] ⎮ { (tick: number): string }` | `x => ‘${x}‘`                             | Labels for axis. Array of label strings must have same length as major ticks. |
| `hide`      | `boolean`                               | `false`                                   | Don't display axis (it will be used only for coordinate system).              |

*¹`class` and `className` are equivalent.*

#### Methods

- `get type(): string`  
  Return passed `type` prop; 'x' or 'y'.
- `get reference(): number`  
  Return passed `reference` prop.
- `scale(value: number, reverse: boolean = false): number`  
  Convert number from axis coordinate to absolute viewport coordinate. If `reverse` is true,
  function convert absolute to axis coordinates.

#### DOM structure

```
g.axis.cartesian > (path.grid.major + path.grid.minor + path.reference + g.labels > text*)
```


### LinearLine component :id=apiref-linearline

Renders data series as X-Y linear line. Use
[&lt;CartesianLine /&gt;](api.md?id=apiref-cartesianline) under the hood.

#### Properties

| Prop     | Type      | Default | Description                                                    |
| -------- | --------- | ------- | -------------------------------------------------------------- |
| `class`¹ | `string`  | none    | Space-separated list of the classes applied to line container. |
| `series` | `string`  | none    | Defines a key to series in the data object, if specified.      |
| `line`   | `boolean` | `true`  | Draw line.                                                     |
| `area`   | `boolean` | `false` | Draw area filled to 'y' axis reference.                        |

*¹`class` and `className` are equivalent.*

Any other passed property will be added to `LinearLine` container. This can be useful if you need to
set some SVG attributes.

#### DOM structure

```
svg.plot.cartesian.line > (path.area + path.line)
```


### CardinalLine component :id=apiref-cardinalline

Renders data series as X-Y line with Catmull-Rome spline interpolation.
Use [&lt;CartesianLine /&gt;](api.md?id=apiref-cartesianline) under the hood.

#### Properties

| Prop      | Type      | Default | Description                                                    |
| --------- | --------- | ------- | -------------------------------------------------------------- |
| `class`¹  | `string`  | none    | Space-separated list of the classes applied to line container. |
| `series`  | `string`  | none    | Defines a key to series in the data object, if specified.      |
| `tension` | `number`  | 1       | Affects how sharply the curve bends control points (0..1).     |
| `line`    | `boolean` | `true`  | Draw line.                                                     |
| `area`    | `boolean` | `false` | Draw area filled to 'y' axis reference.                        |

*¹`class` and `className` are equivalent.*

Any other passed property will be added to `CardinalLine` container. This can be useful if you need to
set some SVG attributes.

#### DOM structure

```
svg.plot.cartesian.line > (path.area + path.line)
```


### BarLine component :id=apiref-barline

Renders data series as discrete bars.

#### Properties

| Prop     | Type      | Default | Description                                                    |
| -------- | --------- | ------- | -------------------------------------------------------------- |
| `class`¹ | `string`  | none    | Space-separated list of the classes applied to line container. |
| `series` | `string`  | none    | Defines a key to series in the data object, if specified.      |

*¹`class` and `className` are equivalent.*

Any other passed property will be added to `BarLine` container. This can be useful if you need to
set some SVG attributes.

#### DOM structure

```
svg.plot.cartesian.bar > path.bars
```


## Polar chart components

### RadialAxis component :id=apiref-radialaxis

Define radial axis (r) of polar charts. Use [&lt;PolarAxis /&gt;](api.md?id=apiref-polaraxis) under
the hood.

#### Properties

| Prop        | Type                                    | Default                                | Description                                                                   |
| ----------- | --------------------------------------- | -------------------------------------- | ----------------------------------------------------------------------------- |
| `class`¹    | `string`                                | none                                   | Space-separated list of the classes applied to axis container.                |
| `min`       | `number` (required)                     |                                        | Low axis limit.                                                               |
| `max`       | `number` (required)                     |                                        | High axis limit.                                                              |
| `reference` | `number`                                | `0`                                    | Reference value (origin).                                                     |
| `step`      | `number`                                | *calculated from `min` and `max`*      | Step between consequence major ticks.                                         |
| `divisor`   | `number`                                | `5`                                    | How many minor ticks are in one major tick.                                   |
| `major`     | `boolean`                               | `false`                                | Show major grid.                                                              |
| `minor`     | `boolean`                               | `false`                                | Show minor grid.                                                              |
| `labels`    | `string[] ⎮ { (tick: number): string }` | `x => ‘${x}‘`                          | Labels for axis. Array of label strings must have same length as major ticks. |
| `hide`      | `boolean`                               | `false`                                | Don't display axis (it will be used only for coordinate system).              |

*¹`class` and `className` are equivalent.*

#### Methods

- `get type(): string`  
  Return passed `type` prop; 'x' or 'y'.
- `get center(): { x: number; y: number }`  
  Return absolute chart center coordinate.
- `get radius(): number`  
  Return chart absolute radius in pixels.
- `scale(value: number, reverse: boolean = false): number`  
  Convert number from polar axis coordinate to absolute viewport coordinate. If `reverse` is true,
  function convert absolute to axis coordinates.

#### DOM structure

```
g.axis.polar > (path.grid.major + path.grid.minor + g.labels > text*)
```


### AngularAxis component :id=apiref-angularaxis

Define angular (theta) axis of polar charts. Use [&lt;PolarAxis /&gt;](api.md?id=apiref-polaraxis)
under the hood.

#### Properties

| Prop        | Type                                    | Default                                | Description                                                                   |
| ----------- | --------------------------------------- | -------------------------------------- | ----------------------------------------------------------------------------- |
| `class`¹    | `string`                                | none                                   | Space-separated list of the classes applied to axis container.                |
| `min`       | `number` (required)                     |                                        | Low axis limit.                                                               |
| `max`       | `number` (required)                     |                                        | High axis limit.                                                              |
| `reference` | `number`                                | `0`                                    | Reference value (origin).                                                     |
| `step`      | `number`                                | *calculated from `min` and `max`*      | Step between consequence major ticks.                                         |
| `divisor`   | `number`                                | `5`                                    | How many minor ticks are in one major tick.                                   |
| `major`     | `boolean`                               | `false`                                | Show major grid.                                                              |
| `minor`     | `boolean`                               | `false`                                | Show minor grid.                                                              |
| `labels`    | `string[] ⎮ { (tick: number): string }` | `x => ‘${x}‘`                          | Labels for axis. Array of label strings must have same length as major ticks. |
| `hide`      | `boolean`                               | `false`                                | Don't display axis (it will be used only for coordinate system).              |

*¹`class` and `className` are equivalent.*

#### Methods

- `get type(): string`  
  Return passed `type` prop; 'x' or 'y'.
- `get center(): { x: number; y: number }`  
  Return absolute chart center coordinate.
- `get radius(): number`  
  Return chart absolute radius in pixels.
- `scale(value: number, reverse: boolean = false): number`  
  Convert number from polar axis coordinate to absolute viewport coordinate. If `reverse` is true,
  function convert absolute to axis coordinates.

#### DOM structure

```
g.axis.polar > (path.grid.major + path.grid.minor + g.labels > text*)
```


### PolarLine component :id=apiref-polarline

Renders data series as linear line on polar chart.

#### Properties

| Prop          | Type                                    | Default  | Description                                                    |
| ------------- | --------------------------------------- | -------- | -------------------------------------------------------------- |
| `class`¹      | `string`                                | none     | Space-separated list of the classes applied to line container. |
| `series`      | `string`                                | none     | Defines a key to series in the data object, if specified.      |
| `interpolate` | `{ (points: ChartSeriesData): string }` | `linear` | Function for interpolation.                                    |

*¹`class` and `className` are equivalent.*

Any other passed property will be added to `PolarLine` container. This can be useful if you need to
set some SVG attributes.

#### DOM structure

```
svg.plot.polar.line > path.line
```


### PolarSector component :id=apiref-polarsector

Renders circular sectors on polar chart. Each data point represents start-end tuple on angular axis.

#### Properties

| Prop     | Type     | Default | Description                                                    |
| -------- | -------- | ------- | -------------------------------------------------------------- |
| `class`¹ | `string` | none    | Space-separated list of the classes applied to line container. |
| `series` | `string` | none    | Defines a key to series in the data object, if specified.      |
| `inner`  | `number` | `0`     | Inner radius of sector on radial axis.                         |
| `outer`  | `number` | `100`   | Outer radius of sector on radial axis.                         |

*¹`class` and `className` are equivalent.*

Any other passed property will be added to `PolarSector` container. This can be useful if you need to
set some SVG attributes.

#### DOM structure

```
svg.plot.polar.sector > path.element*
```


## Interaction components

### BoxZoom component :id=apiref-boxzoom

Enables drawing zoom box with pointer device over chart canvas. Event `onLimits` return array with dragged rect in format `[x0, y0, x1, y1]`. Single click without dragging return `null`.

#### Properties

| Prop        | Type                            | Default | Description                                    |
| ----------- | ------------------------------- | ------- | ---------------------------------------------- |
| `restrict`  | `'x' ⎮ 'y'`                     | none    | Optionally restrict to 'x' or 'y' axis.        |
| `modifiers` | `string`                        |         | Required modifiers¹.                           |
| `onLimits`  | `{ (limits?: number[]): void }` | none    | Callback, that is called if range is selected. |

*¹Modifiers that must be pressed for the component pass the `wheel` events to `host` element. String
of `ctrl`, `alt`, and `shift` joined with `+`.*

#### DOM structure

```
g.box-zoom > (path.backdrop + path.outline)
```


### WheelZoom component :id=apiref-wheelzoom

Enables zooming with wheel or touchpad.

#### Properties

| Prop        | Type                            | Default | Description                                    |
| ----------- | ------------------------------- | ------- | ---------------------------------------------- |
| `modifiers` | `string`                        |         | Required modifiers¹.                           |
| `onLimits`  | `{ (limits?: number[]): void }` | none    | Callback, that is called if range is selected. |

*¹Modifiers that must be pressed for the component pass the `wheel` events to `host` element. String
of `ctrl`, `alt`, and `shift` joined with `+`.*


### Pan component :id=apiref-pan

Enables moving with chart axis.

#### Properties

| Prop        | Type                            | Default | Description                                    |
| ----------- | ------------------------------- | ------- | ---------------------------------------------- |
| `modifiers` | `string`                        |         | Required modifiers¹.                           |
| `onLimits`  | `{ (limits?: number[]): void }` | none    | Callback, that is called if range is selected. |

*¹Modifiers that must be pressed for the component pass the `wheel` events to `host` element. String
of `ctrl`, `alt`, and `shift` joined with `+`.*


### Tooltip component :id=apiref-tooltip

Tooltip is helper component for displaying tooltips. It accepts single children that accepts cursor position and
do actual drawing of tooltip.

#### Props passed to children

| Prop         | Type                       | Description                                        |
| ------------ | -------------------------- | -------------------------------------------------- |
| `position`   | `[number, number]`         | `[x, y]` absolute position of pointer cursor.      |
| `data`       | `ChartData`                | Passed charts series.                              |
| `rect`       | `Rect`                     | Size and absolute position of chart root SVG.      |
| `updateAxis` | `(axis: Axis): void`       | See [Surface](api.md?id=apiref-surface) component. |
| `axes`       | `{ [type: string]: Axis }` | See [Surface](api.md?id=apiref-surface) component. |

Any passed property will be also passed to children.

Props `data` and `rect` are provided by [Chart](api.md?id=apiref-chart) component, `updateAxis`
and `axes` are provided by [Surface](api.md?id=apiref-surface) component.

#### DOM structure

```
g.tooltip
```


## Low-level components

The following components can be used for easier extensibility. None of them implement
the `shouldComponentUpdate` method.


### CartesianAxis component :id=apiref-cartesianaxis

Define generic cartesian axis.

#### Properties

| Prop        | Type                                    | Default                                | Description                                                                   |
| ----------- | --------------------------------------- | -------------------------------------- | ----------------------------------------------------------------------------- |
| `class`¹    | `string`                                | none                                   | Space-separated list of the classes applied to axis container.                |
| `type`      | `'x' ⎮ 'y'` (required)                  |                                        | Type of axis.                                                                 |
| `min`       | `number` (required)                     |                                        | Low axis limit.                                                               |
| `max`       | `number` (required)                     |                                        | High axis limit.                                                              |
| `position`  | `'start' ⎮ 'end'`                       | `'start'` for `'x'`, `'end'` for `'y'` | Position of axis labels.                                                      |
| `reference` | `number` (required)                     |                                        | Reference value (origin).                                                     |
| `scaler`    | `{ (value: number, min: number, max: number, reverse?: boolean): number }` (required) | | Function that translate coordinates into/from chart positions.         |
| `ticks`     | `{ major: number[]; minor: number[] }` (required) |                              | Positions of mirror and majors grid ticks.                                    |
| `major`     | `boolean`                               | `false`                                | Show major grid.                                                              |
| `minor`     | `boolean`                               | `false`                                | Show minor grid.                                                              |
| `labels`    | `string[] ⎮ { (tick: number): string }` | `x => ‘${x}‘`                          | Labels for axis. Array of label strings must have same length as major ticks. |
| `hide`      | `boolean`                               | `false`                                | Don't display axis (it will be used only for coordinate system).              |

*¹`class` and `className` are equivalent.*

#### Methods

- `get type(): string`  
  Return passed `type` prop; 'x' or 'y'.
- `get reference(): number`  
  Return passed `reference` prop.
- `scale(value: number, reverse: boolean = false): number`  
  Convert number from axis coordinate to absolute viewport coordinate. If `reverse` is true,
  function convert absolute to axis coordinates.

#### DOM structure

```
g.axis.cartesian > (path.grid.major + path.grid.minor + path.reference + g.labels > text*)
```


### CartesianLine component :id=apiref-cartesianline

Renders data series as X-Y linear line.

#### Properties

| Prop          | Type                                    | Default  | Description                                                    |
| ------------- | --------------------------------------- | -------- | -------------------------------------------------------------- |
| `class`¹      | `string`                                | none     | Space-separated list of the classes applied to line container. |
| `series`      | `string`                                | none     | Defines a key to series in the data object, if specified.      |
| `interpolate` | `{ (points: ChartSeriesData): string }` | `linear` | Function for interpolation.                                    |
| `line`        | `boolean`                               | `true`   | Draw line.                                                     |
| `area`        | `boolean`                               | `false`  | Draw area filled to 'y' axis reference.                        |

*¹`class` and `className` are equivalent.*

Any other passed property will be added to `CartesianLine` container. This can be useful if you need to
set some SVG attributes.


### PolarAxis component :id=apiref-polaraxis

Define generic polar axis.

#### Properties

| Prop        | Type                                    | Default                                | Description                                                                   |
| ----------- | --------------------------------------- | -------------------------------------- | ----------------------------------------------------------------------------- |
| `class`¹    | `string`                                | none                                   | Space-separated list of the classes applied to axis container.                |
| `type`      | `'r' ⎮ 't'` (required)                  |                                        | Type of axis.                                                                 |
| `min`       | `number` (required)                     |                                        | Low axis limit.                                                               |
| `max`       | `number` (required)                     |                                        | High axis limit.                                                              |
| `scaler`    | `{ (value: number, min: number, max: number, reverse?: boolean): number }` (required) | | Function that translate coordinates into/from chart positions.         |
| `ticks`     | `{ major: number[]; minor: number[] }` (required) |                              | Positions of mirror and majors grid ticks.                                    |
| `major`     | `boolean`                               | `false`                                | Show major grid.                                                              |
| `minor`     | `boolean`                               | `false`                                | Show minor grid.                                                              |
| `labels`    | `string[] ⎮ { (tick: number): string }` | `x => ‘${x}‘`                          | Labels for axis. Array of label strings must have same length as major ticks. |
| `hide`      | `boolean`                               | `false`                                | Don't display axis (it will be used only for coordinate system).              |

*¹`class` and `className` are equivalent.*

#### Methods

- `get type(): string`  
  Return passed `type` prop; 'x' or 'y'.
- `get center(): { x: number; y: number }`  
  Return absolute chart center coordinate.
- `get radius(): number`  
  Return chart absolute radius in pixels.
- `scale(value: number, reverse: boolean = false): number`  
  Convert number from polar axis coordinate to absolute viewport coordinate. If `reverse` is true,
  function convert absolute to axis coordinates.

#### DOM structure

```
g.axis.polar > (path.grid.major + path.grid.minor + g.labels > text*)
```


### Pointer component :id=apiref-pointer

Helper component, that register `pointerdown` or `pointerenter` event to passed `host` element according passed
handlers. After `pointerdown`/`pointerenter` event is raised, component register `pointermove`
and `pointerup`/`pointerleave` to `window`.

#### Properties

Pointer component has 2 forms, depending on whether it should react to active pointer events (click) or hover events.
Only one of onPointerDown/onPointerEnter and onPointerUp/onPointerLeave methods is currently supported.

Active events form:

| Prop            | Type                              | Default | Description                                      |
| --------------- | --------------------------------- | ------- | ------------------------------------------------ |
| `host`          | `HTMLElement` (required)          |         | Element to registred `pointerdown` event.        |
| `modifiers`     | `string`                          |         | Required modifiers¹.                             |
| `onPointerDown` | `{ (event: PointerEvent): void }` | none    | Callback, that is called on `pointerdown` event. |
| `onPointerMove` | `{ (event: PointerEvent): void }` | none    | Callback, that is called on `pointermove` event. |
| `onPointerUp`   | `{ (event: PointerEvent): void }` | none    | Callback, that is called on `pointerup` event.   |

Hover events form:

| Prop             | Type                              | Default | Description                                       |
| ---------------- | --------------------------------- | ------- | ------------------------------------------------- |
| `host`           | `HTMLElement` (required)          |         | Element to registred `pointerenter` event.        |
| `modifiers`      | `string`                          |         | Required modifiers¹.                              |
| `onPointerEnter` | `{ (event: PointerEvent): void }` | none    | Callback, that is called on `pointerenter` event. |
| `onPointerMove`  | `{ (event: PointerEvent): void }` | none    | Callback, that is called on `pointermove` event.  |
| `onPointerLeave` | `{ (event: PointerEvent): void }` | none    | Callback, that is called on `pointerleave` event. |

*¹Modifiers that must be pressed for the component pass the `wheel` events to `host` element. String
of `ctrl`, `alt`, and `shift` joined with `+`.*


### Wheel component :id=apiref-wheel

Helper component, that register `wheel` event to passed `host` element.

#### Properties

| Prop        | Type                            | Default | Description                                |
| ----------- | ------------------------------- | ------- | ------------------------------------------ |
| `host`      | `HTMLElement` (required)        |         | Element to registred `wheel` event.        |
| `modifiers` | `string`                        |         | Required modifiers¹.                       |
| `onWheel`   | `{ (event: WheelEvent): void }` | none    | Callback, that is called on `wheel` event. |

*¹Modifiers that must be pressed for the component pass the `wheel` events to `host` element. String
of `ctrl`, `alt`, and `shift` joined with `+`.*


## Low-level functions

The following functions can be used for easier extensibility.

### Interpolation functions

- `function linear(points: ChartSeriesData): string`  
  Generate path definition of linear line as series of `L` commands.
- `function cardinal(points: ChartSeriesData, tension?: number): string;`  
  Generate path definition of cubic Bézier curve as series of `C` commands.


### Scaler functions

- `function linearScaler(value: number, min: number, max: number, reverse?: boolean): number;`  
  Normalize value between min and max to value in range 0 to 1 on linear scale (min..max => 0..1).
  If `reverse` is `true`, conversion is inverted (0..1 => min..max).
- `function logScaler(value: number, min: number, max: number, reverse?: boolean): number;`  
  Normalize value between min and max to value in range 0 to 1 on logarithm scale
  (min..max => 0..1). If `reverse` is `true`, conversion is inverted (0..1 => min..max).


### Grid step estimation functions

- `function estimateUniformStep(min: number, max: number, count?: number): number;`  
  Return step `(max - min) / count`.
- `function estimateLinearStep(min: number, max: number, count?: number, dividers?: number[]): number;`  
  Finds a step value `dividers * 10^n` such that the main grid has the next lower number of `count`
  divisions.
- `function estimateLogStep(min: number, max: number, base?: number, count?: number): number;`  
  Finds a step value from logaritgm scale (like ...0.1, 1, 10... for base 10) such that the main
  grid has the next lower number of `count` divisions.


### Functions for generate grid ticks

- `function generateLinearTicks(min: number, max: number, step: number, divisor: number, reference: number, closed?: boolean): { major: number[]; minor: number[] };`  
  Generate object with major and minor tick with linear scale.
- `function generateLogTicks(min: number, max: number, step: number, divisor: number, reference: number, base?: number): { major: number[]; minor: number[] };`  
  Generate object with major and minor tick with logarithm scale.
