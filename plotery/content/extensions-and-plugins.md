# Extensions and plugins

## How Plotery work?

Plotery components provide their own props along with other services and values to all `children`
via `cloneElement`. This way components in `<Plotery>` get access to `data` props passed
to `<Plotery>` and other essentials props.

You can place any own component to `<Plotery>` or `<Surface>`, use `data`, `axes`, and other props
and emit SVG DOM. See what props and methods are passed to children in [API](api.md).


## Custom axes

Main function of the axis is providing method(s) for conversion between absolute viewport
coordinates and axis coordinates. Axis also usually draw grid and labels.

Axis must be registered via `updateAxis` prop and provides `scale` method, `type` property and
possibly other methods/properties that are needed for conversion. 


## Custom symbols

Custom symbols, such as bars, lines, or slices can be created as any other Preact component.
Generally symbols components take `data`, `series`, and `axes` props, calculate points in viewport
coordinates and render some SVG shapes. Coordinates can be calculated like this:

```js
const points = series ? data[series] : data;
const scaled = points.map(x => [axes.x.scale(x[0]), axes.y.scale(x[1])]);
```

You can use [&lt;CartesianLine /&gt;](api.md?id=apiref-cartesianline) if you need render data
series as X-Y line.

!> Please note, that some props can not be present in the initial render. In such cases return
`null` and wait for the next render.


## Example: highlight sections

Next component render color box in chart. It takes x and y values, calculates absolute viewport
coordinate and add SVG rect to chart.

```jsx
const Rect = ({ rect, axes, x1, x2, y1, y2, ...others }) => {
	if (!axes.x || !axes.y) {
		return null;
	}
	const c = [
		x1 == null ? 0 : axes.x.scale(x1),
		x2 == null ? rect.width : axes.x.scale(x2),
		y2 == null ? 0 : axes.y.scale(y2),
		y1 == null ? rect.height : axes.y.scale(y1)
	];
	return <rect x={c[0]} y={c[2]} width={c[1] - c[0]} height={c[3] - c[2]} ...{others} />;
};
```

Then, you can use `<Rect />` plugin directly in Plotery chart:

```jsx
<Chart data={hist}>
	<LinearAxis type="x" min={-15} max={15} major />
	<LinearAxis type="y" min={0} max={100} major />
	<Rect y1={60} y2={90} opacity="0.1" fill="brown" />
	<Rect x1={-10} x2={-5} opacity="0.05" />
	<BarLine />
</Chart>
```

```preview
import { html, render } from 'htm/preact';
import { Chart, LinearAxis, BarLine } from '@shelacek/plotery';
const hist = Array.from(
	{ length: 30 },
	(_, i) => [i - 14.5, 82 * Math.exp(-Math.pow(i / 6 - 3, 2))]
);
const Rect = ({ rect, axes, x1, x2, y1, y2, ...others }) => {
	if (!axes.x || !axes.y) {
		return null;
	}
	const c = [
		x1 == null ? 0 : axes.x.scale(x1),
		x2 == null ? rect.width : axes.x.scale(x2),
		y2 == null ? 0 : axes.y.scale(y2),
		y1 == null ? rect.height : axes.y.scale(y1)
	];
	return html`<rect x=${c[0]} y=${c[2]} width=${c[1] - c[0]} height=${c[3] - c[2]} ...${others} />`;
};
const App = () => html`
	<${Chart} data=${hist}>
		<${LinearAxis} type="x" min=${-15} max=${15} major />
		<${LinearAxis} type="y" min=${0} max=${100} major />
		<${Rect} y1=${60} y2=${90} opacity="0.15" fill="orange" />
		<${Rect} x1=${-10} x2=${-5} opacity="0.1" />
		<${BarLine} />
	<//>
`;
render(html`<${App} />`, document.body);
```
