# Styling

Plotery can be styled and animated with CSS,
[SVG attributes](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/Presentation) and
possibly with [SMIL](https://developer.mozilla.org/en-US/docs/Web/SVG/SVG_animation_with_SMIL).
Don't listen mad Chrome developers, SMIL isn't death.

Please consult "DOM structure" of Plotery components in [API](api.md) for CSS selectors.

## Default stylesheet

Default stylesheet for Plotery is included in `dist/scss/*.scss`. You can redefine any Plotery color
by overriding the corresponding SCSS variable. Please see
https://bitbucket.org/shelacek/plotery/src/master/styles/ for reference.

?> Static transpilled stylesheet can be included from `dist/css/plotery.css`.

If variable `$plotery-include-classes` is set to `false`, no Plotery classes are emitted and you can
use mixins to roll your own classes.

## Example: using defs element

The example below show how to apply gradients on chart, add dots to each data point (except first and
last) and use masking for fading effect:

```jsx
<Chart data={points}>
	<defs>
		<linearGradient id="chart-fade" x1="0" y1="0" x2="1" y2="0">
			<stop offset="0" stop-color="black"></stop>
			<stop offset="0.1" stop-color="white"></stop>
			<stop offset="0.9" stop-color="white"></stop>
			<stop offset="1" stop-color="black"></stop>
		</linearGradient>
		<linearGradient id="chart-area" x1="0" x2="0" y1="0" y2="1">
			<stop offset="0" stop-color="var(--base-color)" stop-opacity="1"></stop>
			<stop offset="0.5" stop-color="#42b983" stop-opacity="0.5"></stop>
			<stop offset="1" stop-color="#7098e0" stop-opacity="0"></stop>
		</linearGradient>
		<linearGradient id="chart-stroke" x1="0" y1="0" x2="0" y2="1">
			<stop offset="0" stop-color="var(--base-color)"></stop>
			<stop offset="0.5" stop-color="#42b983"></stop>
			<stop offset="1" stop-color="#7098e0"></stop>
		</linearGradient>
		<mask id="chart-mask" maskContentUnits="objectBoundingBox">
			<rect x="0" y="-1" width="1" height="2" fill="url(#chart-fade)"/>
		</mask>
		<marker id="chart-dot" viewBox="0 0 10 10" refX="5" refY="5" markerWidth="5" markerHeight="5">
			<circle cx="5" cy="5" r="2" fill="var(--base-background-color)" stroke="var(--base-color)" stroke-width="2" />
		</marker>
	</defs>
	<style>
		.plotery > svg {
			mask: url(#chart-mask);
		}
		.plotery .plot.gradient .line {
			marker-mid: url(#chart-dot);
			stroke: url(#chart-stroke);
			stroke-width: 3px;
		}
		.plotery .plot.gradient .area {
			fill: url(#chart-area);
		}
	</style>
	<LinearAxis type="x" min={0} max={6} hide />
	<LinearAxis type="y" min={0} max={100} hide />
	<CardinalLine className="gradient" area />
</Chart>
```

...and here is the result...

```preview
import { html, render } from 'htm/preact';
import { Chart, LinearAxis, CardinalLine } from '@shelacek/plotery';
const points = [[0, 24], [1, 5], [2, 98], [3, 39], [4, 48], [5, 38], [6, 43]];
const App = () => html`
	<${Chart} data=${points}>
		<defs>
			<linearGradient id="chart-fade" x1="0" y1="0" x2="1" y2="0">
				<stop offset="0" stop-color="black"></stop>
				<stop offset="0.1" stop-color="white"></stop>
				<stop offset="0.9" stop-color="white"></stop>
				<stop offset="1" stop-color="black"></stop>
			</linearGradient>
			<linearGradient id="chart-area" x1="0" x2="0" y1="0" y2="1">
				<stop offset="0" stop-color="var(--base-color)" stop-opacity="1"></stop>
				<stop offset="0.5" stop-color="#42b983" stop-opacity="0.5"></stop>
				<stop offset="1" stop-color="#7098e0" stop-opacity="0"></stop>
			</linearGradient>
			<linearGradient id="chart-stroke" x1="0" y1="0" x2="0" y2="1">
				<stop offset="0" stop-color="var(--base-color)"></stop>
				<stop offset="0.5" stop-color="#42b983"></stop>
				<stop offset="1" stop-color="#7098e0"></stop>
			</linearGradient>
			<mask id="chart-mask" maskContentUnits="objectBoundingBox">
				<rect x="0" y="-1" width="1" height="2" fill="url(#chart-fade)"/>
			</mask>
			<marker id="chart-dot" viewBox="0 0 10 10" refX="5" refY="5" markerWidth="5" markerHeight="5">
				<circle cx="5" cy="5" r="2" fill="var(--base-background-color)" stroke="var(--base-color)" stroke-width="2" />
			</marker>
		</defs>
		<style>
			.plotery > svg {
				mask: url(#chart-mask);
			}
			.plotery .plot.gradient .line {
				marker-mid: url(#chart-dot);
				stroke: url(#chart-stroke);
				stroke-width: 3px;
			}
			.plotery .plot.gradient .area {
				fill: url(#chart-area);
			}
		</style>
		<${LinearAxis} type="x" min=${0} max=${6} hide />
		<${LinearAxis} type="y" min=${0} max=${100} hide />
		<${CardinalLine} className="gradient" area />
	<//>
`;
render(html`<${App} />`, document.body);
```

!> Please note, that [style](https://developer.mozilla.org/en-US/docs/Web/SVG/Element/style) and
[defs](https://developer.mozilla.org/en-US/docs/Web/SVG/Element/defs) isn't encapsulated
to containing SVG element. You can use
[shadow DOM](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_shadow_DOM)
to isolate it, pull the definition out into a static SVG, or reference all with unique IDs.
