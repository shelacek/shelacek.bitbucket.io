# Building forms

Formica consist of `<Form />` and controls `<FormGroup />`, `<FormArray />`, `<FormControl />`
and `<FormAction />`. Form control can be also native *form* input like `<input>` or `<select>`.

`<Form />` wrap native `<form>` element and expose *form* model to it's children. `<FormGroup />`,
`<FormArray />`, and `<FormAction />` doesn't introduce any DOM elements itself.


## Form groups

`<FormGroup />` represents the basic building block. It selects property from *form* model by
it's `name` prop and connect this submodel to actual controls, like `<input>`, but also another
`<FormGroup />`, `<FormArray />` or `<FormControl />`.

If I have following model:

```ts
type FormModel = { first: string; second: { third: string; fourth: string; } };
```

Then model in `<FormGroup name="second">` is:

```ts
type SubModel = { third: string; fourth: string; };
```

FormGroup without `name` prop passes whole parent model to it's childs. This can be useful
especially if we want to control `disabled` props for several items without having them in
a submodel, or for automatic mapping in subforms.


## Form arrays

`<FormArray />` receive array and for every item render and connect its children. `<FormArray />`
can also receive the model object. In this case, it selects property by `name` prop like `<FormGroup />`
does.

`<FormAction />` allow simple *form* model modifications, like adding/removing items of the array.


### Add/remove items

Let's say we have the following model:

```ts
type FormModel = { contacts: { email: string; }[]; };
```

And appropriate form that use `FormAction` to add/remove array items can look like:

```jsx
<Form value={form} onChange={this.handleChange} onSubmit={this.handleSubmit}>
	<FormArray name="contacts">
		<div class="row">
			<label>Email: <input name="email" type="email" /></label>
			<FormAction>
				{({ remove }) => (<button type="button" onClick={remove}>Remove</button>)}
			</FormAction>
		</div>
	</FormArray>
	<div class="row end">
		<FormAction name="contacts" item={{ email: '' }}>
			{({ add }) => (<button type="button" onClick={add}>Add email</button>)}
		</FormAction>
		<input type="submit" />
	</div>
</Form>
```

```preview
import { html, render, Component } from 'htm/preact';
import { Form, FormArray, FormAction } from '@shelacek/formica';
class App extends Component {
	state = { form: { contacts: [{ email: 'first@example.com' }, { email: 'second@example.com' }] } };
	handleChange = form => this.setState({ form });
	handleSubmit = event => alert(JSON.stringify(this.state.form, null, '\t'));
	render({}, { form }) {
		return (
			html`<${Form} value=${form} onChange=${this.handleChange} onSubmit=${this.handleSubmit}>
				<${FormArray} name="contacts">
					<div class="row">
						<label>Email: <input name="email" type="email" /></label>
						<${FormAction}>
							${({ remove }) => html`<button type="button" onClick=${remove}>Remove</button>`}
						<//>
					</div>
				<//>
				<div class="row end">
					<${FormAction} name="contacts" item=${{ email: '' }}>
						${({ add }) => html`<button type="button" onClick=${add}>Add email</button>`}
					<//>
					<button type="submit">Submit form</button>
				</div>
			<//>`
		);
	}
}
render(html`<${App} />`, document.body);
```

If you need only display array without adding/removing items, simply do not use `FormAction`s.

?> Like always, lf you need more complex form modifications, roll your own component. You can take
inspiration from `FormAction`
[code](https://bitbucket.org/shelacek/formica/src/master/src/controls/form-action.component.jsx).


## Form controls

`<FormControl />` is a helper component, that exposes `disabled`, `touched` and `validity` to enable
displaying of validation message. It also associate label tag with input field and expose some CSS
classes. If you don't need such functionality and simple `:invalid` CSS pseudo-class is enough,
you can ignore `<FormControl />` entirely.

The `<FormControl />` wraps its child elements into a single `<div>` to which it applies CSS classes
`disabled`, `touched` and `invalid`.

```jsx
<FormControl name="q">
	<label>Search on DuckDuckGo</label>
	<input type="search" required />
</FormControl>
```

```preview
import { html, render, Component } from 'htm/preact';
import { Form, FormControl } from '@shelacek/formica';
class App extends Component {
	state = { form: { q: 'HTML Inputs and Labels: A Love Story' } };
	handleChange = form => this.setState({ form });
	handleSubmit = () => window.open(`https://duckduckgo.com/?q=${encodeURIComponent(this.state.form.q)}`, '_blank');
	render({}, { form }) {
		return (
			html`<${Form} value=${form} onChange=${this.handleChange} onSubmit=${this.handleSubmit}>
				<${FormControl} name="q" class="form-input">
					<label>Search on DuckDuckGo</label>
					<input type="search" required />
				<//>
			<//>`
		);
	}
}
render(html`<${App} />`, document.body);
```

If you clicked to label, input gets focus automatically. Also try delete text in input and then
click away - input should turn red after first blur.


## Subforms

Subforms in Formica is created as custom contols with use `asFormControl` HOC. `FormGroup` can be
conveniently used for automatic linking it's child controls.

```jsx
import { FormGroup, asFormControl } from '@shelacek/formica';

const InfoForm = asFormControl(props => (
	<FormGroup {...props}>
		<label>Bio: <textarea name="bio" maxLength="250" /></label>
		<label>Website: <input name="website" type="text" pattern="(http://|https://)\\S{1,63}" /></label>
	</FormGroup>
));
```

The use is quite straightforward:

```jsx
<Form value={form} onChange={this.handleChange} onSubmit={this.handleSubmit}>
	<label>Name: <input name="name" type="text" required /></label>
	<InfoForm name="info"></InfoForm>
	<button type="submit">Submit form</button>
</Form>
```

This will create exactly the same form as on the [title page](/?id=what-about-example).

## Disable controls

Any Formica control, like native fields, has `disabled` prop, that can be used to disable single
field or group of fields.

Next example show form's `useBilling` checkbox controlling FormGroup `disabled` prop:

```jsx
<Form value={form} onChange={this.handleChange} onSubmit={this.handleSubmit}>
	<label>Name: <input name="name" type="text" required /></label>
	<label>Addres: <textarea name="lines" required /></label>
	<label><input name="useBilling" type="checkbox" /> Use billing address</label>
	<FormGroup name="billing" disabled={!form.useBilling}>
		<label>Name: <input name="name" type="text" required /></label>
		<label>Addres: <textarea name="lines" required /></label>
	</FormGroup>
	<button type="submit">Submit form</button>
</Form>
```

```preview
import { html, render, Component } from 'htm/preact';
import { Form, FormGroup } from '@shelacek/formica';
class App extends Component {
	state = { form: { name: 'Homer J. Simpson', address: '742 Evergreen Terrace\nSpringfield, NT 49007', useBilling: false, billing: { name: '', address: ''} } };
	handleChange = form => this.setState({ form });
	handleSubmit = event => event.target.checkValidity() && alert(JSON.stringify(this.state.form, null, '\t'));
	render({}, { form }) {
		return (
			html`<${Form} class="validated" value=${form} onChange=${this.handleChange} onSubmit=${this.handleSubmit}>
				<label>Name: <input name="name" type="text" required /></label>
				<label>Addres: <textarea name="address" required /></label>
				<label class="checkbox"><input name="useBilling" type="checkbox" /> Use billing address</label>
				<${FormGroup} name="billing" disabled=${!form.useBilling}>
					<label>Name: <input name="name" type="text" required /></label>
					<label>Addres: <textarea name="address" required /></label>
				<//>
				<button type="submit">Submit form</button>
			<//>`
		);
	}
}
render(html`<${App} />`, document.body);
```
