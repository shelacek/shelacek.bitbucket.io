![logo](../logomark.svg)

# Formica <small>2.0.0</small>

> Finally simple forms for Preact!

<a href="javascript:window.scrollTo({ top: window.innerHeight, behavior: 'smooth' });">Documentation</a>
[Bitbucket](https://bitbucket.org/shelacek/formica)
