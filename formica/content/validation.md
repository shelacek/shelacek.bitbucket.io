# Validation

**Formica** uses browser's native constraint validation, so you can use `:invalid` CSS pseudo-class
to highlight invalid fields and `event.target.checkValidity()` method in `onSubmit` handler.

If you want display invalid fields after blur ([Validate on blur or keypress?]), you can wrap
*form* field by `<FormControl />`, that apply CSS class `.touched` after the first blur.

[Validate on blur or keypress?]: https://ux.stackexchange.com/questions/48268/validate-on-blur-or-keypress


## Validation messages

You can use `<FormControl />` also for displaying validation errors:

```jsx
<FormControl name="age" class="form-input">
	{({ touched, validity }) => (
		<label>Enter your age</label>
		<input type="number" required min="15" />
		{touched && validity.valueMissing && <span>This field is required</span>}
		{touched && validity.rangeUnderflow && <span>Sorry, you must be at least 15 years old</span>}
	)}
</FormControl>
```
```preview
import { html, render, Component } from 'htm/preact';
import { Form, FormControl } from '@shelacek/formica';
class App extends Component {
	state = { form: {} };
	handleChange = form => this.setState({ form });
	render({}, { form }) {
		return (
			html`<${Form} value=${form} onChange=${this.handleChange}>
				<${FormControl} name="age" class="form-input">
					${({ touched, validity }) => (
						html`<label>Enter your age</label>
							<input type="number" required min="15" />
							${touched && validity.valueMissing && html`<span>This field is required</span>`}
							${touched && validity.rangeUnderflow && html`<span>Sorry, you must be at least 15 years old</span>`}`
					)}
				<//>
			<//>`
		);
	}
}
render(html`<${App} />`, document.body);
```
