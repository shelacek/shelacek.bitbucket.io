# Getting started

Formica is Preact library that can be run in a browser and supports SSR.


## Instalation

Formica can be installed with NPM or YARN:

```bash
npm i @shelacek/formica
# or
yarn add @shelacek/formica
```

You can also obtain the library from CDN like [jsDelivr](https://www.jsdelivr.com/package/npm/@shelacek/formica)
or [unpkg](https://unpkg.com/@shelacek/formica/).


## Integration

Formica can be used with bundlers like Webpack. This is usually the case because JSX needs to be
transpiled to javascript before it can be run in the browser.

All components are available on the top-level export:

```js
import { Form, FormGroup, FormArray /* ... */ } from '@shelacek/formica';
```


### Use without bundlers

Formica does not need to be transpiled, so you can alternatively load it from CDN and
execute directly in the browser without any build step.

?> You can check [HTM](https://github.com/developit/htm). HTM is useful if you don't use JSX and
still prefer JSX-like syntax instead of plain javascript. This is the route, that this site use.


## First form

This is full code, that create form from the [title page](/?id=what-about-example):

```jsx
// import used components/functions
import { h, render, Component } from 'preact';
import { Form, FormGroup } from '@shelacek/formica';

class ProfileForm extends Component {
	state = { form: null };

	componentDidMount() {
		this.setState({
			form: /* await fetch(...).json() */ {
				name: 'John Doe',
				info: {
					bio: 'Hello, I\'m John Doe and ...',
					website: 'http://example.com'
				}
			}
		});
	}

	// choose your favorite binding method
	handleChange = form => { 
		this.setState({ form });
	};

	handleSubmit = event => {
		if (event.target.checkValidity()) {
			/* fetch(..., { body: JSON.stringify(this.state.form) }) */
			alert(JSON.stringify(this.state.form, null, '\t'));
		}
	};

	render({}, { form }) {
		return (
			<Form class="validated" value={form} onChange={this.handleChange} onSubmit={this.handleSubmit}>
				<label>Name: <input name="name" type="text" required /></label>
				<FormGroup name="info">
					<label>Bio: <textarea name="bio" maxLength="250" /></label>
					<label>Website: <input name="web" type="text" pattern="(http://|https://)\S{1,63}" /></label>
				</FormGroup>
				<button type="submit">Submit form</button>
			</Form>
			<pre>{JSON.stringify(form, null, '\t')}</pre>
		);
	}
}

// render component into document
render(<ProfileForm />, document.body);
```

Try it on [CodeSandbox](https://codesandbox.io/s/formica-demo-9vv3n).


## Hooks

Formica doesn't force you to use hooks, but using hooks is totally legal. If you're still hyped about
this thingy, you can write previous example like this:

```jsx
import { h, render } from "preact";
import { useState } from "preact/hooks";
import { Form, FormGroup } from "@shelacek/formica";

function ProfileForm() {
	const [form, setForm] = useState({ /* name: ... */ });
	const handleSubmit = event => { /* ... */ };
	return (
		<Form class="validated" value={form} onChange={setForm} onSubmit={handleSubmit}>
		{/* ... */}
	);
}

render(<ProfileForm />, document.body);
```

?> I know, `handleSubmit` can be wrapped into `useCallback` hook, but `useCallback` comes with
a cost. As always, use your tools responsibly.


## Signals

Yes! For storing form state, you can also use [Preact Signals](https://preactjs.com/guide/v10/signals).

```jsx
import { h, render } from "preact";
import { signal } from "@preact/signals";
import { Form, FormGroup } from "@shelacek/formica";

const form = signal({ /* name: ... */ });
const handleSubmit = event => { /* ... */ };

const ProfileForm = () => (
	<Form class="validated" value={form.value} onChange={x => form.value = x} onSubmit={handleSubmit}>
	{/* ... */}
);

render(<ProfileForm />, document.body);
```
