# API

For full API of exported symbols in dense format, please see typings at
https://bitbucket.org/shelacek/formica/src/master/src/formica.d.ts.

Form components:

- [Form component](#apiref-form)
- [FormGroup component](#apiref-formgroup)
- [FormArray component](#apiref-formarray)
- [FormControl component](#apiref-formcontrol)
- [FormAction component](#apiref-formaction)

Higher-order components:

- [asFormControl HOC](#apiref-asformcontrol)


## Form components

### Form component :id=apiref-form

Encapsulates `<form>` element and also wraps children into name-less `<FormGroup />`.


#### Properties

| Prop       | Type                       | Description                                                   |
| ---------- | -------------------------- | ------------------------------------------------------------- |
| `value`    | `any`                      | Form data (form model).                                       |
| `onChange` | `{ (value: any): void }`   | Form data changes handler.                                    |
| `onSubmit` | `{ (event: Event): void }` | Form submit handler.                                          |

Any other passed properties will be added to `<form>` element.


### FormGroup component :id=apiref-formgroup

`<FormGroup />` groups another form controls and connect them with the submodel.


#### Properties

| Prop       | Type      | Description                                           |
| ---------- | --------- | ----------------------------------------------------- |
| `disabled` | `boolean` | Disable descendant controls.                          |
| `name`     | `string`  | Selected *form* model property to expose as submodel. |

`<FormGroup />` accepts standard vnodes or function as children. The function is called with an object
contains props passed to `<FormGroup />`, like `name` or `data-index`, `disabled` and `value`.
This can be useful if you need display index of array subform or some metadata carried
with subform value.


### FormArray component :id=apiref-formarray

`<FormArray />` iterate descendant controls by received submodel's array.


#### Properties

| Prop       | Type      | Description                                                                    |
| ---------- | --------- | ------------------------------------------------------------------------------ |
| `keyedBy`  | `string`  | Name of *form* model property to use as `<FormArray />`'s item key.            |
| `disabled` | `boolean` | Disable descendant controls.                                                   |
| `name`     | `string`  | Selected *form* model property to iterate. Array is expected if not specified. |


### FormControl component :id=apiref-formcontrol

`<FormControl />` wrap and connect native `<input>`, `<select>` or `<textarea>`. It can also
associate label tag and expose some CSS classes and props for validation - namely `disabled`, `touched`
and `invalid`.


#### Properties

| Prop       | Type      | Description                                                                     |
| ---------- | --------- | ------------------------------------------------------------------------------- |
| `id`       | `string`  | Id mapped to control's `id` and label's `for`. Default is `form-control-${cnt}` |
| `class`¹   | `string`  | Space-separated list of the classes.                                            |
| `disabled` | `boolean` | Disable control.                                                                |
| `name`     | `string`  | Selected model property to connect with native input.                           |

*¹`class` and `className` are equivalent.*

`<FormControl />` accepts standard vnodes or function as children. The function is called with an object
contains `disabled`, `touched` and `validity`. Argument `validity` is [ValidityState] object,
others are booleans.

[ValidityState]: https://developer.mozilla.org/en-US/docs/Web/API/ValidityState


### FormAction component :id=apiref-formaction

`<FormAction />` can be used for simple form mutations and expose `add` and `remove` functions
to children render prop. Any of these functions can be called directly from `onClick` events
and the like.


#### Properties

| Prop       | Type                  | Description                                         |
| ---------- | --------------------- | --------------------------------------------------- |
| `name`     | `string`              | Optional *form* submodel to attach.                 |
| `item`     | `{ (): void } \| any` | Item value or function that return value to append. |

`<FormAction />` accepts function as children which can takes object with 2 functions:

- `add` function add `item` to selected (or parent) submodel array, or object.
- `remove` function remove self from selected (or parent) submodel array, or object. Prop `name`
   can specify key to remove.

This object also contains all props, that `<FormAction />` received.


## Higher-Order Components

### asFormControl HOC :id=apiref-asformcontrol

Enhance any component with `name`, `value` and `onChange` props into *form* control.

It has only one argument: the wrapped component.

```ts
function asFormControl<P>(WrappedControl: ComponentFactory<P>): ComponentFactory<P>;
```
