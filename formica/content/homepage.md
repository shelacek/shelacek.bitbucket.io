# Formica <!-- {docsify-ignore-all} -->

> Finally simple forms for Preact!

*Formica* allows you to build fairly complex forms declaratively in JSX. Nested forms and arrays
are supported out of the box. The library is trying to not re-invent the wheel so it combines
native forms API with controlled inputs (native isn't that bad). Validation is handled by
the browser's constraint validation. Also, *Formica* has no dependencies and has a small footprint!


## Features

- Close to native: write forms like you do in plain HTML
- Reactive: the data in the form are those that are in the component state and vice versa
- Freedom: you can directly use native inputs, create custom inputs or incorporate third party ones
- Use this.state, hooks or signals, class components or functional components. Your project, your
  rules!
- [Light as a feather](https://bundlephobia.com/package/@shelacek/formica): only ~2.2kiB mingzipped
  and without dependencies (except Preact)


## What about example?

Example of some basic form can look like:

```jsx
<Form value={form} onChange={this.handleChange} onSubmit={this.handleSubmit}>
	<label>Name: <input name="name" type="text" required /></label>
	<FormGroup name="info">
		<label>Bio: <textarea name="bio" maxLength="250" /></label>
		<label>Website: <input name="web" type="text" pattern="(http://|https://)\S{1,63}" /></label>
	</FormGroup>
	<button type="submit">Submit form</button>
</Form>
```

```preview
import { html, render, Component } from 'htm/preact';
import { Form, FormGroup } from '@shelacek/formica';
class ProfileForm extends Component {
	state = { form: { name: 'John Doe', info: { bio: 'Hello, I\'m John Doe and ...', website: 'http://example.com' } } };
	handleChange = form => this.setState({ form });
	handleSubmit = event => event.target.checkValidity() && alert(JSON.stringify(this.state.form, null, '\t'));
	render({}, { form }) {
		return (
			html`<${Form} class="validated" value=${form} onChange=${this.handleChange} onSubmit=${this.handleSubmit}>
				<label>Name: <input name="name" type="text" required /></label>
				<${FormGroup} name="info">
					<label>Bio: <textarea name="bio" maxLength="250" /></label>
					<label>Website: <input name="website" type="text" pattern="(http://|https://)\\S{1,63}" /></label>
				<//>
				<button type="submit">Submit form</button>
			<//>
			<pre>${JSON.stringify(form, null, '\t')}</pre>`
		);
	}
}
render(html`<${ProfileForm} />`, document.body);
```

Try it on [CodeSandbox](https://codesandbox.io/s/formica-demo-9vv3n). For more complex example take
a look at [Caesar cipher demo](https://codesandbox.io/s/formica-caesar-cipher-demo-hrlii).
