# Custom form controls

You can create custom controls. **Formica** map any vnode, that is enhanced
with `asFormControl` [Higher-Order Components].

Custom *form* controls receive these props:

| Prop       | Type                                     | Description                        |
| ---------- | ---------------------------------------- | ---------------------------------- |
| `name`     | `string \| number` (optional)            | Name of connected model property.  |
| `disabled` | `boolean`                                | Disable control.                   |
| `value`    | `any`                                    | Value of connected model property. |
| `onChange` | `{ (event: OnChangeEvent<any>): void }`  | Value changes handler.             |

!> Please note: `name` prop can be also a numeric index in case, that the control is used directly
as an array item field.

Prop's `onChange` event can proxy native form input event or implement it's minimal subset:

```ts
type OnChangeEvent<T> = Event | {
	target: {
		name: string | number; // same as props.name
		value: T; // new mapped value
	}
};
```

## Custom control example

Minimal example of custom control:

```jsx
import { h } from 'preact';
import { asFormControl } from "@shelacek/formica";

export const FormInput = asFormControl(({ children, ...attrs }) => (
	<label>
		{children}
		<input {...attrs} />
	</label>
));
```

[![Edit formica-simple-custom-control](https://codesandbox.io/static/img/play-codesandbox.svg)](https://codesandbox.io/s/formica-simple-custom-control-63142?fontsize=14)


## 3rd party components

If 3rd party control use `value` and `onChange` props to control it's state, all that's needed is
a wrap it to `asFormControl`. This should be a case with most of components/libraries.

When component use another props or it's behavior is different, some glue logic may be needed:

```jsx
import { h } from 'preact';
import { asFormControl } from "@shelacek/formica";
import { AmazingInput as RainbowAmazingInput } from "rainbow-unicorns-library";

export const AmazingInput = asFormControl(({ value, onChange, ...attrs }) => (
	<RainbowAmazingInput content={value} onContent={onChange} {...attrs}></RainbowAmazingInput>
));
```


[Higher-Order Components]: https://reactjs.org/docs/higher-order-components.html
