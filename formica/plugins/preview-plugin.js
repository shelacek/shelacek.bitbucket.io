import htm from 'https://esm.sh/htm@3.1.1';
import * as preact from 'https://esm.sh/preact@10.22.0';
import * as formica from 'https://esm.sh/@shelacek/formica@2.0.0?deps=preact@10.22.0';

const htmPreact = { ...preact, html: htm.bind(preact.h) };

const replaceMap = [
	[
		/import \{(.+?)\} from 'htm\/preact'/,
		'const {$1} = htmPreact'
	], [
		/import \{(.+?)\} from '@shelacek\/formica'/,
		'const {$1} = formica'
	], [
		/render\((.+?), document\.body\)/,
		'render($1, el)'
	]
];

const selector = 'pre[data-lang="preview"]';

export const previewPlugin = hook => hook.doneEach(() => {
	for (const target of document.querySelectorAll(selector)) {
		const content = replaceMap.reduce(
			(acc, x) => acc.replace(...x),
			target.querySelector('code').textContent
		);
		const el = document.createElement('div');
		el.classList.add('preview');
		target.parentNode.replaceChild(el, target);
		const func = new Function(
			'htmPreact',
			'formica',
			'el',
			content
		);
		func(htmPreact, formica, el);
	}
});
