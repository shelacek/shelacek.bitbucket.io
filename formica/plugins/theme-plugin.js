const moon = `<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M21 12.8A9 9 0 1 1 11.2 3a7 7 0 0 0 9.8 9.8z"/></svg>`;
const sun = `<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="5"/><line x1="12" y1="1" x2="12" y2="3"/><line x1="12" y1="21" x2="12" y2="23"/><line x1="4.2" y1="4.2" x2="5.6" y2="5.6"/><line x1="18.4" y1="18.4" x2="19.8" y2="19.8"/><line x1="1" y1="12" x2="3" y2="12"/><line x1="21" y1="12" x2="23" y2="12"/><line x1="4.2" y1="19.8" x2="5.6" y2="18.4"/><line x1="18.4" y1="5.6" x2="19.8" y2="4.2"/></svg>`;

export const themePlugin = hook => {
	let button;

	const updateButton = (isDark, system = false) => {
		const title = system
			? `Switch to system (${isDark ? 'light' : 'dark'}) scheme`
			: `Switch to ${isDark ? 'light' : 'dark'} scheme`;
		button.innerHTML = isDark ? sun : moon;
		button.setAttribute('title', title);
	}

	const onPrefersColorSchemeChanged = e => {
		window.localStorage.removeItem('theme');
		document.documentElement.classList.toggle('dark', e.matches);
		updateButton(e.matches);
	};

	const onButtonClicked = () => {
		const isDark = document.documentElement.classList.toggle('dark');
		const isPrefersDark = window.matchMedia
			&& window.matchMedia('(prefers-color-scheme: dark)').matches;
		if (isPrefersDark == isDark) {
			window.localStorage.removeItem('theme');
		}
		else {
			window.localStorage.setItem('theme', isDark ? 'dark' : 'light');
		}
		updateButton(isDark, isPrefersDark != isDark);
	};

	hook.mounted(() => {
		window.matchMedia && window
			.matchMedia('(prefers-color-scheme: dark)')
			.addEventListener('change', onPrefersColorSchemeChanged);
	});

	hook.doneEach(() => setTimeout(() => {
		if (document.querySelector('.theme-switcher')) {
			return;
		}
		button = document.createElement('button');
		button.classList.add('theme-switcher');
		button.addEventListener('click', onButtonClicked);
		updateButton(
			document.documentElement.classList.contains('dark'),
			window.localStorage.getItem('theme')
		)
		let container = document.querySelector('.app-nav');
		if (!container) {
			container = document.querySelector('.content');
		}
		container.prepend(button);
	}));
};
