export const logoPlugin = hook => hook.mounted(() => {
	const img = document.querySelector('.app-name img');
	const xhr = new XMLHttpRequest();
	xhr.open('GET', img.getAttribute('src'));
	xhr.overrideMimeType("image/svg+xml");
	xhr.onload = () => img.parentNode.replaceChild(xhr.responseXML.documentElement, img);
	xhr.send();
});

